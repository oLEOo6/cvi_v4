﻿using CVI_Lib;
using CVI_v4.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Serialization;

namespace CVI_v4.ViewModels
{
    class MainWindowVM
    {
        public ObservableCollection<CaseVM> Case_Set { get; set; } = new ObservableCollection<CaseVM>();

        public String Name_View { get; set; }
        public String Port_View { get; set; }
        public CaseVM SelectedCase { get; set; }

        public MainWindowVM()
        {
            FileStream fs = new FileStream("Save.xml", FileMode.Open);
            // 以序列化物件的型別為參數，建立 XmlSerializer 物件
            XmlSerializer xSerializer = new XmlSerializer(typeof(ObservableCollection<CaseVM>));
            // 執行 XmlSerializer.Deserialize
            Case_Set = (ObservableCollection<CaseVM>)xSerializer.Deserialize(fs);
            fs.Close();
        }

        #region Command
        public ICommand ICommand_Exit { get { return new RelayCommand(Exit, CanExecute); } }
        public void Exit()
        {
            //Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            //// 移除指定的appSettings
            //config.AppSettings.Settings.Remove("CELL");
            //// 新增指定的appSettings
            //config.AppSettings.Settings.Add("CELL", CELL.ToString());

            //config.Save(ConfigurationSaveMode.Modified);

            // 建立儲存的檔案
            FileStream fs = new FileStream("Save.xml", FileMode.Create);
            // 以序列化物件的型別為參數，建立 XmlSerializer 物件
            XmlSerializer xSerializer = new XmlSerializer(typeof(ObservableCollection<CaseVM>));
            // 執行 XmlSerializer.Serialize
            xSerializer.Serialize(fs, Case_Set);
            fs.Close();

            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~EXIT~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        }
        public ICommand ICommand_Add { get { return new RelayCommand(Add, CanExecute); } }
        public void Add()
        {
            Case_Set.Add(new CaseVM(Name_View ,Convert.ToInt32(Port_View)));
        }
        public ICommand ICommand_Delete { get { return new RelayCommand(Delete, CanExecute); } }
        public void Delete()
        {
            SelectedCase.Close();
            Case_Set.Remove(SelectedCase);
        }

        public bool CanExecute()
        {
            return true;
        }
        #endregion
    }
}
