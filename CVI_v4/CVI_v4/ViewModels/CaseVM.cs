﻿using CVI_Lib;
using CVI_v4.Models;
using MahApps.Metro.Actions;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Xml.Serialization;

namespace CVI_v4.ViewModels
{
    public class CaseVM : ViewModelBase
    {
        [XmlIgnore]
        public bool Run = true;//Flag
        #region Pack Setting
        public int CELL { get; set; } = 11;
        public int PACK { get; set; } = 8;
        public int TEMP { get; set; } = 12;
        #endregion
        #region UI Setting
        public double Voltage_outlier { get; set; } = 15;
        public double Voltage_dynamic_outlier { get; set; } = 60;
        public double Voltage_high { get; set; } = 4.2;
        public double Voltage_low { get; set; } = 2.8;
        public double Temperature_high { get; set; } = 50;
        public double Temperature_low { get; set; } = 0;

        public bool Light { get; set; } = false;
        public String Password { get; set; } = "79983949";

        //Log
        public String LogPath { get; set; } = String.Empty;
        public String LogName { get; set; } = String.Empty;
        public UInt16 Log_Interval { get; set; } = 1;
        [XmlIgnore]
        public System.Timers.Timer Timer_Log;
        [XmlIgnore]
        private int LogCount = 0;
        [XmlIgnore]
        public bool LogFlag { get; set; } = false;
        [XmlIgnore]
        private String LogPathName;
        #endregion
        #region Data
        public String Name { get; set; }
        [XmlIgnore]
        public Battery Battery { get; set; }
        [XmlIgnore]
        public ObservableCollection<Package> PackCollection { get; set; }
        [XmlIgnore]
        public double PackVoltageMax { get; set; }
        [XmlIgnore]
        public double PackVoltageMin { get; set; }
        [XmlIgnore]
        public bool PackVoltageDif { get; set; }
        #endregion
        #region Socket
        private Socket socket;
        public int Port { get; set; }

        public Task Loop;//Receive loop  ::  Testing??
        #endregion

        public CaseVM() : this(null, 0) {}
        public CaseVM(String name ,int port)
        {
            Name = name;
            Port = port;
            Battery = new Battery();
            PackCollection = new ObservableCollection<Package>();
            
            socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
            
            Timer_Log = new System.Timers.Timer();
            Timer_Log.Interval = Log_Interval * 1000;
            Timer_Log.Elapsed += new ElapsedEventHandler(LogFC);

            for (int i = 1; i < PACK + 1; i++)
                PackCollection.Add(new Package(i));

            Loop = new Task(Looop);
            Loop.Start();
        }
        public void Looop()
        {
            while (true)
            {
                //Flag stop loop
                if (!Run)
                    //throw new Exception("Stop Task!");
                    break;

                try
                {
                    if (!socket.Connected)
                        socket.Connect("127.0.0.1", Port);
                    byte[] buffer = new byte[256];
                    socket.Receive(buffer);

                    Light = !Light;
                    Protocol(buffer);

                    Thread.Sleep(100);
                }
                catch (Exception err)
                {
                    Console.WriteLine("Loop Exception:\n" + err.Message);
                    Thread.Sleep(1000);
                }
            }
            Close();
        }

        public void Protocol(byte[] input)
        {
            if (input[0] == 0x01)
            {
                Battery.Voltage = ((UInt16)(input[1] + (input[2] << 8)) / 100.0);
                Battery.Current = ((Int16)(input[3] + (input[4] << 8)) / 100.0);
                Battery.Temperature = (Int16)(input[5] + (input[6] << 8)) / 100.0;
                Battery.Soc = input[7];
                Battery.Errorcode = input[16];
                Battery.Status = (UInt16)(input[31] + (input[32] << 8));
                Battery.Relay = input[33];
            }
            else if (input[0] == 0x02)
            {
                for (int i = 0; i < PACK; i++)
                {
                    for (int j = 0; j < CELL; j++)
                        PackCollection[i].Voltage.Cell[j].Val = (input[i * CELL * 2 + j * 2 + 1] + (input[i * CELL * 2 + j * 2 + 2] << 8)) / 10000.0;

                    double tV = 0, tVmax = double.MinValue, tVmin = double.MaxValue;
                    for (int j = 0; j < CELL; j++)
                    {
                        tV += PackCollection[i].Voltage.Cell[i].Val;
                        tVmax = tVmax > PackCollection[i].Voltage.Cell[j].Val ? tVmax : PackCollection[i].Voltage.Cell[j].Val;
                        tVmin = tVmin < PackCollection[i].Voltage.Cell[j].Val ? tVmin : PackCollection[i].Voltage.Cell[j].Val;
                    }
                    PackCollection[i].Voltage.Total = tV;   //cell總電壓
                    PackCollection[i].Voltage.Max = tVmax;  //cell最大電壓
                    PackCollection[i].Voltage.Min = tVmin;  //cell最小電壓
                    PackCollection[i].Voltage.Dif = tVmax - tVmin;  //cell電壓差

                    double Vref;
                    if (Battery.Current < 0.2 && Battery.Current > -0.2)
                        Vref = Voltage_outlier;
                    else
                        Vref = Voltage_dynamic_outlier;

                    if (tVmax - tVmin > (Vref / 1000.0))    //Dif__Flag
                        PackCollection[i].Voltage.Dif_flag = true;
                    else
                        PackCollection[i].Voltage.Dif_flag = false;

                    //20200428 新增Pack電壓
                    double ttVmax = double.MinValue, ttVmin = double.MaxValue;
                    for (int j = 0; j < PACK; j++)
                    {
                        ttVmax = ttVmax > PackCollection[j].Voltage.Total ? ttVmax : PackCollection[j].Voltage.Total;
                        ttVmin = ttVmin < PackCollection[j].Voltage.Total ? ttVmin : PackCollection[j].Voltage.Total;
                    }
                    PackVoltageMax = ttVmax;
                    PackVoltageMin = ttVmin;
                    if (ttVmax - ttVmin > (Vref / 1000.0))    //Dif__Flag
                        PackVoltageDif = false;
                    else
                        PackVoltageDif = true;
                }

            }
            else if (input[0] == 0x04)
            {
                for (int i = 0; i < PACK; i++)
                {
                    for (int j = 0; j < TEMP; j++)
                        PackCollection[i].Temperature.Cell[j].Val = (Int16)(input[i * TEMP * 2 + j * 2 + 1] + (input[i * TEMP * 2 + j * 2 + 2] << 8)) / 100.0;

                    double tT = 0, tTmax = double.MinValue, tTmin = double.MaxValue;
                    for (int j = 0; j < TEMP; j++)
                    {
                        tT += PackCollection[i].Temperature.Cell[j].Val;
                        tTmax = tTmax > PackCollection[i].Temperature.Cell[j].Val ? tTmax : PackCollection[i].Temperature.Cell[j].Val;
                        tTmin = tTmin < PackCollection[i].Temperature.Cell[j].Val ? tTmin : PackCollection[i].Temperature.Cell[j].Val;
                    }
                    PackCollection[i].Temperature.Average = tT / TEMP;   //cell平均溫度
                    PackCollection[i].Temperature.Max = tTmax;  //cell最高溫度
                    PackCollection[i].Temperature.Min = tTmin;  //cell最低溫度
                    PackCollection[i].Temperature.Dif = tTmax - tTmin;  //cell溫度差
                }
            }
        }

        public void FileCreate()
        {
            try
            {
                if (LogPath == String.Empty)
                    LogPath = Environment.CurrentDirectory + @"\Log";
                if (LogName == String.Empty)
                    LogName = "Log";
                /////新增資料夾
                if (!Directory.Exists(LogPath))
                    Directory.CreateDirectory(LogPath);

                int k = 1;
                LogPathName = LogPath + @"\" + LogName + "-" + DateTime.Now.ToString("yyyy-MM-dd") + "_" + k + ".csv";
                while (File.Exists(LogPathName))
                {
                    k++;
                    LogPathName = LogPath + @"\" + LogName + "-" + DateTime.Now.ToString("yyyy-MM-dd") + "_" + k + ".csv";
                }

                var sw = new StreamWriter(LogPathName, true);

                //Log Head
                string head = "TimeStamp,SOC,Voltage,Current,Status,Relay Status,";
                for (int i = 1; i < PACK + 1; i++)
                {
                    for (int j = 1; j < CELL + 1; j++)
                        head += "P" + i + "V.CELL-" + j + ",";
                    head += "P" + i + "V.Total," + "P" + i + "V.Min," + "P" + i + "V.Max," + "P" + i + "V.Dif";

                    for (int j = 1; j < TEMP + 1; j++)
                        head += "P" + i + "T.CELL-" + j + ",";
                    head += "P" + i + "T.Average," + "P" + i + "T.Min," + "P" + i + "T.Max," + "P" + i + "T.Dif";
                }
                sw.WriteLine(head);
                sw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Method FILEname Error!\n" + e.Message);
            }
        }
        public void LogFC(object sender, System.Timers.ElapsedEventArgs e)
        {
            Console.WriteLine(LogPathName);
            StreamWriter sw = new StreamWriter(LogPathName, true);///////////////////////////////////////////

            String Daa = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "," +
                        Battery.Soc + "," +
                        Battery.Voltage + "," +
                        Battery.Current + "," +
                        Battery.Status + "," +
                        Battery.Relay + ",";

            for (int i = 0; i < PACK; i++)
            {
                for (int j = 0; j < CELL; j++)
                    Daa += PackCollection[i].Voltage.Cell[j].Val + ",";
                Daa += PackCollection[i].Voltage.Total + "," + PackCollection[i].Voltage.Min + "," + PackCollection[i].Voltage.Max + "," + PackCollection[i].Voltage.Dif + ",";

                for (int j = 0; j < TEMP; j++)
                    Daa += PackCollection[i].Temperature.Cell[j].Val + ",";
                Daa += PackCollection[i].Temperature.Average + "," + PackCollection[i].Temperature.Min + "," + PackCollection[i].Temperature.Max + "," + PackCollection[i].Temperature.Dif + ",";
            }
            sw.WriteLine(Daa);

            //Log Size Control
            LogCount++;
            if (LogCount > 49999)
            {
                LogCount = 0;
                FileCreate();
                Console.WriteLine("50000 datas!\nChange file.");
            }

            sw.Close();
        }
        public void PathSelect()
        {
            //File Select
            //var openFileDialog = new Microsoft.Win32.OpenFileDialog();
            //var result = openFileDialog.ShowDialog();
            //if (result == true)
            //{
            //    var s = openFileDialog.FileName;
            //    Console.WriteLine(s);
            //}

            //Path Select
            var openBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            openBrowserDialog.SelectedPath = LogPath;
            openBrowserDialog.ShowDialog();
            LogPath = openBrowserDialog.SelectedPath;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////Command////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Command
        public ICommand ICommand_Log { get { return new RelayCommand(LogMd, CanExecute); } }
        public void LogMd()
        {
            if (!LogFlag)
            {
                PathSelect();
                FileCreate();
                Timer_Log.Interval = Log_Interval * 1000;
                Timer_Log.Start();
                LogFlag = true;
                Console.WriteLine(LogFlag);
            }
            else
            {
                Timer_Log.Stop();
                LogFlag = false;
                Console.WriteLine(LogFlag);
            }
        }

        public bool CanExecute()
        {
            return true;
        }
        #endregion
        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////Command////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////

        public void Close()
        {
            try
            {
                Run = false;
                socket.Close();
            }
            catch(Exception error)
            {
                Console.WriteLine("Close Error!\n" + error.Message);
            }
        }
    }
}
