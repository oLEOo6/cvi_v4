using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_Lib
{
    //Status Enum
    public enum Battery_Status
    {
        GOOD = 0x0000,
        OV = 0x0001,
        UV = 0x0002,
        OT_C = 0x0004,
        OT_D = 0x0008,
        UT_C = 0x0010,
        UT_D = 0x0020,
        OC_C = 0x0040,
        OC_D = 0x0080,
        ERROR = 0x0100,
        SC = 0x0200,
        IR_Lv1 = 0x0400,
        IR_Lv2 = 0x0800,
        CP_Relay = 0x1000,
        CN_Relay = 0x2000,
        DP_Relay = 0x4000,
        DN_Relay = 0x8000,

        //GOOD = 0,
        //OV,
        //UV,
        //OT_C,
        //OT_D,
        //UT_C,
        //UT_D,
        //OC_C,
        //OC_D,
        //ERROR,
        //SC,
        //IR_Lv1,
        //IR_Lv2,
        //CP_Relay,
        //CN_Relay,
        //DP_Relay,
        //DN_Relay,
    }

    class Enums
    {
    }
}
