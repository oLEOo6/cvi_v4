﻿using CVI_Lib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_v4.Models
{
    public class Voltage : ViewModelBase
    {
        //2020/06/23 test
        private DoubleValue[] doubleValues = new DoubleValue[12];
        public ObservableCollection<DoubleValue> Cell { get; set; } = new ObservableCollection<DoubleValue>();
        public Voltage()
        {
            for(int i = 0; i < 12; i++)
            {
                doubleValues[i] = new DoubleValue();
                Cell.Add(doubleValues[i]);
            }
        }
        public double Max { get; set; }
        public double Min { get; set; }
        public double Total { get; set; }
        public double Dif { get; set; }
        public bool Dif_flag { get; set; }
    }
}
