﻿using CVI_Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_v4.Models
{
    public class Battery : ViewModelBase
    {
        public double Voltage { get; set; }
        public double Current { get; set; }
        public double Temperature { get; set; }
        public byte Soc { get; set; }
        public UInt16 Status { get; set; }
        public byte Errorcode { get; set; }
        public byte Relay { get; set; }
        public UInt16 BMSVersion { get; set; }
        public UInt16 FIRMWAREVersion { get; set; }
    }
}
