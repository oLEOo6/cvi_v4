﻿using CVI_Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_v4.Models
{
    public class Package
    {
        public int Index { get; set; }
        public Voltage Voltage { get; set; } = new Voltage();
        public Temperature Temperature { get; set; } = new Temperature();

        public Package() { }
        public Package(int index)
        {
            this.Index = index;
        }
    }
}
