﻿using CVI_Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVI_v4.Models
{
    public class DoubleValue : ViewModelBase
    {
        //private double val;
        //public double Val { get { return val; } set { val = value; OnPropertyChanged(); } }
        public double Val { get; set; }
    }
}
