﻿using CVI_Lib;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CVI_v4.Converters
{
    class StatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            UInt16 status = (UInt16)value;
            String temp = "";

            if (status == 0) return "Good!";

            foreach (int sss in Enum.GetValues(typeof(Battery_Status)))
            {
                if (sss == 0) continue;
                if ((status & sss) == sss)
                    temp += Enum.GetName(typeof(Battery_Status), sss) + "  ";
            }
            return temp;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
