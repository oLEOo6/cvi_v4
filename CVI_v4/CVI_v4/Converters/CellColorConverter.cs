﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace CVI_v4.Converters
{
    class CellColorConverter : IMultiValueConverter
    {
        //0:Max     1:Min   2:Value
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[2] == System.Windows.DependencyProperty.UnsetValue)
                return Brushes.Black;

            if (System.Convert.ToDouble(values[2]) >= System.Convert.ToDouble(values[0]) || System.Convert.ToDouble(values[2]) <= System.Convert.ToDouble(values[1]))
                return Brushes.Red;
            else
                return Brushes.Green;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
